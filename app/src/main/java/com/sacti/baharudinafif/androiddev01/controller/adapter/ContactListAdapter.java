package com.sacti.baharudinafif.androiddev01.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.sacti.baharudinafif.androiddev01.R;
import com.sacti.baharudinafif.androiddev01.model.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by baharudinafif on 2/14/17.
 */

public class ContactListAdapter extends ArrayAdapter<Contact> {
    private ArrayList<Contact> listContact;
    private int lastPosition = -1;
    private Context mContext;

    // View lookup cache
    private static class ViewHolder {
        TextView name;
        TextView status;
    }

    public ContactListAdapter(Context context, ArrayList<Contact> data) {
        super(context, R.layout.contact_item, data);
        listContact = data;
        mContext = context;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Contact item = getItem(position);
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.contact_item, parent, false);
            viewHolder.name = (TextView) convertView.findViewById(R.id.contactName);
            viewHolder.status = (TextView) convertView.findViewById(R.id.contactStatus);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        lastPosition = position;
        viewHolder.name.setText(item.getName());
        viewHolder.status.setText(item.getStatus());
        viewHolder.status.setTextColor(ContextCompat.getColor(mContext, R.color.grey));
        // Render on screen
        return convertView;
    }
}

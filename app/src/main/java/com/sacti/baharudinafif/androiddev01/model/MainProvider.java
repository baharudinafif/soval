package com.sacti.baharudinafif.androiddev01.model;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by baharudinafif on 3/4/17.
 */

public class MainProvider extends ContentProvider{
    private static final String TAG     = "MainProvider";
    private ContactTable database;

    private static final int CONTACTS   = 1;
    private static final int CONTACT    = 2;

    private static final String BASE_PATH           = "contacts";
    private static final String AUTHORITY           = "com.sacti.baharudinafif.androiddev01.model.MainProvider";
    public static final Uri CONTENT_URI             = Uri.parse("content://"+AUTHORITY+"/"+BASE_PATH);

    private static final UriMatcher uriMatcher      = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        uriMatcher.addURI(AUTHORITY, BASE_PATH, CONTACTS);
        uriMatcher.addURI(AUTHORITY, BASE_PATH + "/#", CONTACT);
    }

    @Override
    public boolean onCreate() {
        database = new ContactTable(getContext());
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

        if(checkColumns(projection)){
            /*
            * Requested column available
            * */
            queryBuilder.setTables(ContactTable.TABLE_NAME);
            int uriType = uriMatcher.match(uri);
            switch (uriType){
                case CONTACTS:
                    break;
                case CONTACT:
                    queryBuilder.appendWhere(ContactTable.COLUMN_ID + "=" + uri.getLastPathSegment());
                default:
                    throw new IllegalArgumentException("Unknown URI: " + uri);
            }
            SQLiteDatabase db   = database.getWritableDatabase();
            Cursor cursor       = queryBuilder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
            cursor.setNotificationUri(getContext().getContentResolver(), uri);
            return cursor;
        }else{
            /*
            * There is (are) column that unavailable
            * */
            throw new IllegalArgumentException("Unknown column(s) in projection");
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        int uriType         = uriMatcher.match(uri);
        SQLiteDatabase  db  = database.getWritableDatabase();
        long id             = 0;
        Log.d(TAG, "insert: " + uriType );
        Log.d(TAG, "insert: " + uri.toString());
        switch (uriType){
            case CONTACTS:
                id  = db.insert(ContactTable.TABLE_NAME, null, values);
                break;
            default:
                throw new IllegalArgumentException("Unknown URI: " + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        Log.d(TAG, "insert: newId => " + id);
        return Uri.parse(CONTENT_URI + "/" + id);
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType         = uriMatcher.match(uri);
        SQLiteDatabase db   = database.getWritableDatabase();
        int rowsDeleted     = 0;
        switch (uriType){
            case CONTACTS:
                rowsDeleted = db.delete(ContactTable.TABLE_NAME, selection, selectionArgs);
                break;
            case CONTACT:
                String id   = uri.getLastPathSegment();
                if(TextUtils.isEmpty(selection)){
                    rowsDeleted = db.delete(ContactTable.TABLE_NAME, ContactTable.COLUMN_ID + "=" + id, null);
                } else {
                    rowsDeleted = db.delete(ContactTable.TABLE_NAME, ContactTable.COLUMN_ID + '=' + id + " and " + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI :" + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsDeleted;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        int uriType         = uriMatcher.match(uri);
        SQLiteDatabase db   = database.getWritableDatabase();
        int rowsUdpated     = 0;
        switch (uriType){
            case CONTACTS:
                rowsUdpated = db.update(ContactTable.TABLE_NAME, values, selection, selectionArgs);
                break;
            case CONTACT:
                String id   = uri.getLastPathSegment();
                if(TextUtils.isEmpty(selection)){
                    rowsUdpated = db.update(ContactTable.TABLE_NAME, values, ContactTable.COLUMN_ID + "=" + id, null);
                }else {
                    rowsUdpated = db.update(ContactTable.TABLE_NAME, values, ContactTable.COLUMN_ID + "=" + id +" and" + selection, selectionArgs);
                }
                break;
            default:
                throw new IllegalArgumentException("Unknown URI :" + uri);
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return rowsUdpated;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    private boolean checkColumns(String[] projection){
        String[] available = {ContactTable.COLUMN_ID, ContactTable.COLUMN_NAME, ContactTable.COLUMN_NUMBER, ContactTable.COLUMN_STATUS};
        if(projection != null){
            HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
            HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));

            if(availableColumns.containsAll(requestedColumns)){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
}

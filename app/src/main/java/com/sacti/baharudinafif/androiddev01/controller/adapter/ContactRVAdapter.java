package com.sacti.baharudinafif.androiddev01.controller.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sacti.baharudinafif.androiddev01.R;
import com.sacti.baharudinafif.androiddev01.model.Contact;

import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by baharudinafif on 2/14/17.
 */

public class ContactRVAdapter extends RecyclerView.Adapter<ContactRVAdapter.ViewHolder> {
    private ArrayList<Contact> listContact;
    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout item;
        public ViewHolder(LinearLayout linLay) {
            super(linLay);
            item = linLay;
        }
    }
    public ContactRVAdapter(Context context, ArrayList<Contact> data) {
        listContact = data;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LinearLayout linLay = (LinearLayout) LayoutInflater.from(context)
                .inflate(R.layout.contact_item, parent, false);
        ViewHolder vh = new ViewHolder(linLay);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        LinearLayout temp = holder.item;
        TextView name = (TextView) temp.findViewById(R.id.contactName);
        TextView status = (TextView) temp.findViewById(R.id.contactStatus);
        name.setText(listContact.get(position).getName());
        status.setText(listContact.get(position).getStatus());
    }

    @Override
    public int getItemCount() {
        return listContact.size();
    }
}

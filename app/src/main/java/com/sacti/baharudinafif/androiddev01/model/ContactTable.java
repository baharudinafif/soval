package com.sacti.baharudinafif.androiddev01.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by baharudinafif on 3/4/17.
 */

public class ContactTable extends SQLiteOpenHelper{
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME       = "contacts";
    public static final String COLUMN_ID        = "id";
    public static final String COLUMN_NAME      = "name";
    public static final String COLUMN_NUMBER    = "number";
    public static final String COLUMN_STATUS    = "status";

    /*
    * database creation
    * */
    private static final String TABLE_CREATION  = "create table " + TABLE_NAME + "( "
            + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_NAME
            + " text not null,"
            + COLUMN_NUMBER
            + " text not null,"
            + COLUMN_STATUS
            + " text not null);";

    public ContactTable(Context context) {
        super(context, TABLE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }
}

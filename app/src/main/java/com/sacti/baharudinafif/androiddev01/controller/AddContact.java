package com.sacti.baharudinafif.androiddev01.controller;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.sacti.baharudinafif.androiddev01.R;
import com.sacti.baharudinafif.androiddev01.model.Contact;
import com.sacti.baharudinafif.androiddev01.model.ContactTable;
import com.sacti.baharudinafif.androiddev01.model.MainProvider;

/**
 * Created by baharudinafif on 3/4/17.
 */

public class AddContact extends AppCompatActivity implements Button.OnClickListener{
    private static final  String  TAG = "AddContact";

    private Context context;
    private Toolbar toolbar;
    private Button saveBtn;
    private Button deleteBtn;
    private EditText name;
    private EditText status;
    private EditText number;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.add_contact);

        toolbar     = (Toolbar) findViewById(R.id.add_contact_toolbar);
        saveBtn     = (Button) findViewById(R.id.add_new_contact);
        deleteBtn   = (Button) findViewById(R.id.delete_contact);
        name        = (EditText) findViewById(R.id.contact_name);
        number      = (EditText) findViewById(R.id.contact_number);
        status      = (EditText) findViewById(R.id.contact_status);

        setSupportActionBar(toolbar);
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_arrow_back );
        }catch (NullPointerException e){
            Log.d(TAG, "onCreate: " + e.getMessage());
        }
        getSupportActionBar().setTitle("Add Contact");
        saveBtn.setText("Add");
        deleteBtn.setText("Clear Contact");
        saveBtn.setOnClickListener(this);
        deleteBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.add_new_contact:
                Log.d(TAG, "onClick: Save Button Clicked");

                /*
                * Get values from input data
                * */
                String newName      = name.getText().toString();
                String newStatus    = status.getText().toString();
                String newNumber    = number.getText().toString();

                /*
                * Untuk mengirim data ke Content Provider, data yg ingin kita kirim harus di bukus
                * dengan ContentValues
                * */
                ContentValues values = new ContentValues();
                values.put(ContactTable.COLUMN_NAME, newName);
                values.put(ContactTable.COLUMN_NUMBER, newNumber);
                values.put(ContactTable.COLUMN_STATUS, newStatus);
                getContentResolver().insert(MainProvider.CONTENT_URI, values);
                finish();
                break;
            case R.id.delete_contact:
                Uri uri = Uri.parse(MainProvider.CONTENT_URI + "");
                getContentResolver().delete(uri, null, null);
                finish();
                break;
        }
    }
}

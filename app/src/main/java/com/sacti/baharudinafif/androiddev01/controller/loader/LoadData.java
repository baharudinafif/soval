package com.sacti.baharudinafif.androiddev01.controller.loader;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.sacti.baharudinafif.androiddev01.controller.HomeActivity;
import com.sacti.baharudinafif.androiddev01.controller.SplashActivity;
import com.sacti.baharudinafif.androiddev01.model.Contact;

import java.net.URL;
import java.util.ArrayList;

/**
 * Created by baharudinafif on 3/4/17.
 */

public class LoadData extends AsyncTask<Context, Void, Integer>{
    private static final String TAG = "LoadData";
    private Context context;
    @Override
    protected Integer doInBackground(Context... params) {
        try {
            context = params[0];
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return 1;
    }

    @Override
    protected void onPostExecute(Integer integer) {
        SplashActivity prevAct = (SplashActivity) context;
        Log.d(TAG, "onPostExecute: Done");
        Intent i = new Intent(prevAct, HomeActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        prevAct.startActivity(i);
    }
}

package com.sacti.baharudinafif.androiddev01.controller;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.sacti.baharudinafif.androiddev01.R;
import com.sacti.baharudinafif.androiddev01.controller.adapter.ContactRVAdapter;
import com.sacti.baharudinafif.androiddev01.model.Contact;
import com.sacti.baharudinafif.androiddev01.model.ContactTable;
import com.sacti.baharudinafif.androiddev01.model.MainProvider;

import java.util.ArrayList;

/**
 * Created by baharudinafif on 2/9/17.
 */

public class TabRight extends Fragment implements LoaderManager.LoaderCallbacks<Object> {
    private static final String TAG = "TabRight";
    private ArrayList<Contact> listContact;
    private RecyclerView contactRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private Context context;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        listContact = new ArrayList<Contact>();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.right, menu);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.tab_right, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        context = getActivity().getApplicationContext();

        retrieveContactData();
        contactRecyclerView= (RecyclerView) getActivity().findViewById(R.id.contact_rv);
        contactRecyclerView.setFocusableInTouchMode(true);
        contactRecyclerView.setFocusable(true);
        mLayoutManager = new LinearLayoutManager(context);
        contactRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new ContactRVAdapter(context, listContact);

        // Performance Improvement
        contactRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_contact:
            {
                Intent i = new Intent(context, AddContact.class);
                startActivity(i);
                return true;
            }
        }
        return false;
    }

    @Override
    public Loader<Object> onCreateLoader(int id, Bundle args) {
        Uri baseUri;
        return null;
    }

    @Override
    public void onLoadFinished(Loader<Object> loader, Object data) {

    }

    @Override
    public void onLoaderReset(Loader<Object> loader) {

    }

    private void retrieveContactData(){
        listContact.clear();
        String[] projection = {
                ContactTable.COLUMN_ID,
                ContactTable.COLUMN_NAME,
                ContactTable.COLUMN_STATUS,
                ContactTable.COLUMN_NUMBER
        };
        Uri uri = Uri.parse(MainProvider.CONTENT_URI + "");
        Cursor cursor   = context.getContentResolver().query(uri, projection, null, null, null);
        if(cursor!=null){
            while(cursor.moveToNext()){
                int id          = cursor.getInt(cursor.getColumnIndexOrThrow(ContactTable.COLUMN_ID));
                String name     = cursor.getString(cursor.getColumnIndexOrThrow(ContactTable.COLUMN_NAME));
                String number   = cursor.getString(cursor.getColumnIndexOrThrow(ContactTable.COLUMN_NUMBER));
                String status   = cursor.getString(cursor.getColumnIndexOrThrow(ContactTable.COLUMN_STATUS));

                Contact c       = new Contact(id, name, number, status);
                listContact.add(c);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        /*
        * STEP
        * 1. Retrieve data from DB
        * 2. Populate data to recycler view
        * */
        retrieveContactData();
        mAdapter = new ContactRVAdapter(context, listContact);
        contactRecyclerView.setAdapter(mAdapter);
        Log.d(TAG, "onResume: CALLED => " + listContact.size());
    }
}

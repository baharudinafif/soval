package com.sacti.baharudinafif.androiddev01.controller;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.sacti.baharudinafif.androiddev01.R;
import com.sacti.baharudinafif.androiddev01.controller.loader.LoadData;
import com.sacti.baharudinafif.androiddev01.model.Contact;
import com.sacti.baharudinafif.androiddev01.model.ContactTable;
import com.sacti.baharudinafif.androiddev01.model.MainProvider;

import java.util.ArrayList;

/**
 * Created by baharudinafif on 3/4/17.
 *
 * Kelas SplashActivity merupakan anak dari AppCompatActivity (extends), hal itu dikarenakan
 * SplashActivity akan menampilkan sebuah UI ke user.
 * UI yg ditampilkan hanyalah sebuah logo, progressbar dan sebuah tulisan yang menadakan
 * sedang dilakukan inisiasi data
 */

public class SplashActivity extends AppCompatActivity{
    private final String TAG    = "SplashActivity";
    private Context context;
    private ImageView logo;
    private ProgressBar bar;
    private TextView loadingText;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        context = this;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LoadData load = new LoadData();
        load.execute(context);
    }
}
